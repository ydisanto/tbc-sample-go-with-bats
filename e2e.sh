#!/bin/bash

# script to run the e2e on your local machine
# requires to install BATS

E2E_FOLDER=e2e

BATS="./${E2E_FOLDER}/bats/bin/bats"

echo "building and installing go app"
if go install; then
  echo "running E2E tests"
  "${BATS}" "${E2E_FOLDER}"
else
  echo "failed to build app"
fi

