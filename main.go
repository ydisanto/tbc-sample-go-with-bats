package main

import (
	"fmt"
	"os"
  "strconv"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Printf("please specify a positive number")
		os.Exit(1)
	}
	arg := os.Args[1]
	i, err := strconv.Atoi(arg)
	if err != nil {
		fmt.Printf("not a positive number: %s", arg)
		os.Exit(1)
	} else if i <= 0 {
		fmt.Printf("not a positive number: %s", arg)
		os.Exit(1)
	}

	fmt.Println(fizzbuzz(i))
}

func fizzbuzz(number int) string {
	multipleOf3 := isMultipleOf3(number)
	multipleOf5 := isMultipleOf5(number)
	if multipleOf3 && multipleOf5 {
		return "fizzbuzz"
	}
	if multipleOf3 {
		return "fizz"
	}
	if multipleOf5 {
		return "buzz"
	}
	return fmt.Sprintf("%d", number)

}

func isMultipleOf3(number int) bool {
	return number % 3 == 0
}

func isMultipleOf5(number int) bool {
	return number % 5 == 0
}
