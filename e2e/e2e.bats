#!/usr/bin/env bats

# defined by CI job or use locally installed libs
BATS_LIBRARIES_DIR=${BATS_LIBRARIES_DIR:-"test_helper"}

setup() {
    load "${BATS_LIBRARIES_DIR}/bats-support/load"
    load "${BATS_LIBRARIES_DIR}/bats-assert/load"
}

@test "no arg display error" {
    run fizzbuzz
    assert_failure
    assert_output 'please specify a positive number'
}

@test "not a number arg display error" {
    run fizzbuzz foo
    assert_failure
    assert_output 'not a positive number: foo'

    run fizzbuzz bar
    assert_failure
    assert_output 'not a positive number: bar'

    run fizzbuzz -25
    assert_failure
    assert_output 'not a positive number: -25'
}


@test "display input number when not multiple of 3 nor 5" {
    run fizzbuzz 1
    assert_success
    assert_output '1'

    run fizzbuzz 2
    assert_success
    assert_output '2'

    run fizzbuzz 4
    assert_success
    assert_output '4'

    run fizzbuzz 7
    assert_success
    assert_output '7'
}

@test "display 'fizz' when input number is multiple of 3 but not of 5" {
    run fizzbuzz 3
    assert_success
    assert_output 'fizz'

    run fizzbuzz 6
    assert_success
    assert_output 'fizz'

    run fizzbuzz 9
    assert_success
    assert_output 'fizz'

    run fizzbuzz 12
    assert_success
    assert_output 'fizz'
}

@test "display 'buzz' when input number is multiple of 5 but not of 3" {
    run fizzbuzz 5
    assert_success
    assert_output 'buzz'

    run fizzbuzz 10
    assert_success
    assert_output 'buzz'

    run fizzbuzz 20
    assert_success
    assert_output 'buzz'

    run fizzbuzz 25
    assert_success
    assert_output 'buzz'
}

@test "display 'fizzbuzz' when input number is multiple of 3 and multiple of 5" {
    run fizzbuzz 15
    assert_success
    assert_output 'fizzbuzz'

    run fizzbuzz 30
    assert_success
    assert_output 'fizzbuzz'

    run fizzbuzz 45
    assert_success
    assert_output 'fizzbuzz'
}
